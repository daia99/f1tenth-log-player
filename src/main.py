import pyglet
from f1tenth_player import F1TenthVideoPlayer
import os

SRC_PATH = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(SRC_PATH, '..')
MAP_PATH = os.path.join(PROJECT_PATH, 'resources', 'maps', 'SILVERSTONE')

if __name__ == "__main__":
    sample_log = os.path.join(PROJECT_PATH, 'resources', 'examples', 'logs', 'multi.jsonl')

    player = F1TenthVideoPlayer(sample_log, MAP_PATH, 1000, 800)
    pyglet.clock.schedule_interval(player.update, 0.005)
    pyglet.app.run()
